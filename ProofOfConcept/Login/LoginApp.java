package Login;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

public class LoginApp extends Application {

    private Stage primaryStage;
    private BorderPane rootLayout;

    @Override
    public void start(Stage primaryStage) {
        this.primaryStage = primaryStage;
        this.primaryStage.setTitle("LoginApp");

        initRootLayout();
        showLoginScreen();
    }

    /**
     * Initializes the root layout.
     */
    public void initRootLayout() {
        try {
            // Load root layout from fxml file.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(LoginApp.class.getResource("RootLayout.fxml"));
            rootLayout = (BorderPane) loader.load();

            // Show the scene containing the root layout.
            Scene scene = new Scene(rootLayout);
            primaryStage.setScene(scene);
            primaryStage.show();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    
    /**
     * Shows the login screen inside the root layout.
     */
    public void showLoginScreen() {
        try {
            // Load loginScreen.
            FXMLLoader loader = new FXMLLoader();
            loader.setLocation(LoginApp.class.getResource("LoginStyle.fxml"));
            AnchorPane loginScreen = (AnchorPane) loader.load();

            // Set person overview into the center of root layout.
            rootLayout.setCenter(loginScreen);
            
            // Give the controller access to the main app.
            LoginController controller = loader.getController();
            controller.setMainApp(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Callback method invoked to notify that a user has been authenticated.
     * Will show the main application screen.
     */ 
    public void authenticated(String sessionID) {
      showMainView(sessionID);
    }
    
    /**
     * showsMainView after login
*/
    private void showMainView(String sessionID) {
		// TODO Auto-generated method stub
    	try {
    	System.out.println("authenthicated");
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(LoginApp.class.getResource("JournalSelector.fxml"));
		AnchorPane mainScreen = (AnchorPane) loader.load();
		rootLayout.setCenter(mainScreen);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	/**

     * Returns the main stage.
     * @return
     */
    public Stage getPrimaryStage() {
        return primaryStage;
    }

    public static void main(String[] args) {
        launch(args);
    }
}

// xmlns="http://javafx.com/javafx/8.0.101"