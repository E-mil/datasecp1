package Login;
import javafx.event.*;
import javafx.fxml.FXML;
import javafx.scene.control.*;

public class LoginController {
@FXML private TextField user;
@FXML private PasswordField password;
@FXML private Button loginButton;	
private LoginApp mainApp;
private static int sessionID = 0;

public void initManager(final LoginApp loginSession) {
    loginButton.setOnAction(new EventHandler<ActionEvent>() {
      @Override public void handle(ActionEvent event) {
        //System.out.println("hej");
    	  String sessionID = authorize();
        if (sessionID != null) {
          loginSession.authenticated(sessionID);
        }
      }
    });
  }

@FXML
public void test(){
	System.out.println("test");
	String sessionID = authorize();
	if (sessionID != null) {
        mainApp.authenticated(sessionID);
      }
}

/**
 * Is called by the main application to give a reference back to itself.
 * 
 * @param mainApp
 */
public void setMainApp(LoginApp mainApp) {
    this.mainApp = mainApp;}


@FXML
private void initialize() {}

@FXML

/**
 * Check authorization credentials.
 * 
 * If accepted, return a sessionID for the authorized session
 * otherwise, return null.
 */   
private String authorize() {
  return 
    "open".equals(user.getText()) && "sesame".equals(password.getText()) 
          ? generateSessionID() 
          : null;
}


private String generateSessionID() {
  sessionID++;
  return "xyzzy - session " + sessionID;
}
}