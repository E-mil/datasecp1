import java.net.*;
import java.io.*;
import javax.net.ssl.*;
import java.security.*;
import java.security.cert.*;
import java.util.Arrays;
import java.util.Scanner;
import java.io.Console;
import java.security.Principal;
import java.security.PrivateKey;
import java.security.UnrecoverableKeyException;
import java.io.FileNotFoundException;

/*
 * This is a proof of concept client
 * Currently just a Patient
 */
public class client {
	private static boolean notAuthorized;
	private static Console cnsl;

	public static void main(String[] args) throws Exception {
		String host = "localhost";
		int port = 9876;
		notAuthorized= true;
		try { /* set up a key manager for client authentication */
			SSLSocketFactory factory = null;
			try {

				  // creates a console object
//		         cnsl = System.console();
//				authorize();
				char[] password = "password".toCharArray();
				KeyStore ks = KeyStore.getInstance("JKS");
				KeyStore ts = KeyStore.getInstance("JKS");
				KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
				TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
				SSLContext ctx = SSLContext.getInstance("TLS");
				ts.load(new FileInputStream("keys/clienttruststore"), password);
				tmf.init(ts); //init truststore
				authorize(ks, kmf);

				ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
				factory = ctx.getSocketFactory();
			} catch (Exception e) {
				e.printStackTrace();
				throw new IOException(e.getMessage());
			}
			SSLSocket socket = (SSLSocket) factory.createSocket(host, port);
			System.out.println("\nsocket before handshake:\n" + socket + "\n");

			/*
			 * send http request
			 *
			 * See SSLSocketClient.java for more information about why there is
			 * a forced handshake here when using PrintWriters.
			 */
			socket.startHandshake();

			SSLSession session = socket.getSession();
			javax.security.cert.X509Certificate cert = session.getPeerCertificateChain()[0];
			String subject = cert.getSubjectDN().getName();
			String issuer = cert.getIssuerDN().getName();
			// System.out.println("certificate name (subject DN field) on
			// certificate received from server:\n" + subject + "\n");
			// System.out.println("certificate name (issuer DN field) on
			// certificate received from server:\n" + issuer + "\n");
			// System.out.println("The certificates serialnumber:
			// "+cert.getSerialNumber());
			// System.out.println("socket after handshake:\n" + socket + "\n");
			System.out.println("secure connection established\n\n");

			BufferedReader read = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
			BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			String msg;

			out.println("Init Server");
			out.flush();


			String hello= in.readLine();
			if (hello.equals("Not authorized connection")){
				in.close();
				out.close();
				read.close();
				socket.close();
				System.exit(0);
			}else{
			byte currentPriviledge = Byte.parseByte(hello); // binary
			System.out.println(currentPriviledge);
			if ((currentPriviledge & 0b1) == 0b1) {
				System.out.println("Has execute access");
			}
			if ((currentPriviledge & 0b10) == 0b10) {
				System.out.println("Has write access");
			}
			if ((currentPriviledge & 0b100) == 0b100) {
				System.out.println("Has read access");
			}

			for (;;) {
				System.out.print(">");
				msg = read.readLine();
				if (msg.equalsIgnoreCase("quit")) {
					break;
				}
				System.out.print("sending '" + msg + "' to server...");
				out.println(msg);
				out.flush();
				System.out.println("message sent");

				System.out.println("response received: '" + parse(in.readLine(),msg) + "' from server\n");
			}
			in.close();
			out.close();
			read.close();
			socket.close();
		}} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private static String parse(String msg,String command){
		String ret = "";
		//msg=msg.replace("\b","\n"); legacy

		if(!command.equals("GET")){
			return msg;
		}
        System.out.println(msg);
		String[] entryDivider = msg.split("divider");

		if(entryDivider.length>1){

			String division = entryDivider[0];
			String modifiable = entryDivider[1];
			String[] div = division.split("ENTRYBREAK");
			for (String s : div ) {
				String[] entry= s.split(", ");
				if(ret.equals("")) ret+="Current division: "+ entry[2] + "\n";
				if (Integer.valueOf(entry[1])==4){
					ret+= entry[0]+"\n";
					String[] rec= entry[3].split(":");
					for (String r : rec) {
						String[] record = r.split(";");
						ret+="\tDoctor: "+ record[2]+"\tNurse: " + record[1] +"\n\tRecord: "+record[3]+"\n";
					}
				}

			}
			ret+= "\n---------------------------\nSucceding entries can be modified\n---------------------------\n";
			String[] mod = modifiable.split("startOfEntry");
			String lastn="";
			for (String s :mod ) {
				if(!s.equals("")){
				String[] entry= s.split(":");
				if (entry[1].equals(lastn)){
					ret+= "\t" + entry[0]+ "\t" +entry[2]+"\n";

				}else{
					ret+= entry[1]+ "\t" +entry[0]+"\t" +entry[2]+"\n";

				}
				lastn = entry[1];
			}
		}


		}
		else{
			//Duplicerad kod, it's bad i know just lazy
			String modifiable = entryDivider[0];
			String[] mod = modifiable.split(":");
            
            String lastn="";
            for (int m = 0;m<mod.length ;m++ ) {
                String[] e = mod[m].split(";");
                if(!e[0].equals(lastn))
			     ret += "\n"+e[0]+"\n";
                 //System.out.println(msg);
                // System.out.println("\n");
                //System.out.println(modifiable);

                ret+="Doctor: "+e[2]+"\tNurse: "+e[1]+"\tRecord: "+e[3]+"\n";
                lastn = e[0];
            
        }

		}



		return ret;
	}

	//authorizes the user
	private static void authorize(KeyStore ks, KeyManagerFactory kmf) throws Exception {
		String usr = "";
		cnsl = System.console();
		while (notAuthorized) {
			usr = cnsl.readLine("Please enter username: ");
			//System.out.println("username is: "+usr + " : len: " + usr.toCharArray().length);
			char[] pwd = cnsl.readPassword("Please enter password: ");
	        //System.out.println("Password is: " + new String(pwd));
			try {
       			String temp = "keys/" + usr; //keystore name = username
       			System.out.println(temp);
       			ks.load(new FileInputStream(temp), pwd); //keystore password = usr password
       			kmf.init(ks, pwd);
       			notAuthorized = false;
       		} catch (FileNotFoundException e) { //if username is wrong
       			System.out.println("You have entered the wrong credentials, please try again.");
       		} catch (IOException e) { //if password is wrong
       			System.out.println("You have entered the wrong credentials, please try again.");
       		}
        }
	}
}
