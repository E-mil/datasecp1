import java.io.*;
import java.net.*;
import java.security.KeyStore;
import javax.net.*;
import javax.net.ssl.*;
import javax.security.cert.X509Certificate;
import java.io.IOException;
import java.nio.file.DirectoryNotEmptyException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.lang.StringBuilder;

public class server implements Runnable {
    private ServerSocket serverSocket = null;
    private static int numConnectedClients = 0;
    private final int ACCESS_READ=4;
    private final int ACCESS_WRITE=2;
    private final int ACCESS_EXECUTE=1;
    private String filepath = "medicalRecord.txt";
    private String divider = "ENTRYBREAK";
    private final int INDEX_NAME = 0;
    private final int INDEX_ACCESS_LEVEL = 1;
    private final int INDEX_DEPARTMENT = 2;
    private final int INDEX_RECORD = 3; //INDEXERING, FUCKING DUMBASS :/
    private final int INDEX_RECORD_PATIENT = 0;
    private final int INDEX_RECORD_NURSE = 1;
    private final int INDEX_RECORD_DOCTOR = 2;
    private final int INDEX_RECORD_PLAINTEXT = 3;
    private final int COMMAND_OLD = 0;
    private final int COMMAND_NEW = 1;
    private final int COMMAND_NAME = 0;
    private final int COMMAND_INDEX = 1;
    private final int COMMAND_PLAINTEXT = 2;

    public server(ServerSocket ss) throws IOException {
        serverSocket = ss;
        newListener();
    }

    /**
    * The main Thread for each instance connected to the server.
    * Does what is does then shuts down.
    */
    public void run() {
        try {
            SSLSocket socket=(SSLSocket)serverSocket.accept();
            newListener();
            SSLSession session = socket.getSession();
            X509Certificate cert = (X509Certificate)session.getPeerCertificateChain()[0];
            String subject = cert.getSubjectDN().getName();
	        String issuer = cert.getIssuerDN().getName();
    	    numConnectedClients++;
            System.out.println("client connected");
            System.out.println(numConnectedClients + " concurrent connection(s)\n");

            PrintWriter out = null;
            BufferedReader in = null;
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));


            //END OF SERVER SETUP
            //----------------------------------------------------------------------


            String subjectName = subject.split(", ")[0].substring(3);
            System.out.println(subjectName);
            boolean[] rights = new boolean[3];
            boolean authorized=false;
            try{setPriviledges(getProperty(subjectName, INDEX_ACCESS_LEVEL),rights);
            authorized=true;}
            catch(Exception e){
              out.println("Not authorized connection");
              in.close();
        			out.close();
        			socket.close();
            	numConnectedClients--;
              System.out.println("client not authorized");
              System.out.println(numConnectedClients + " concurrent connection(s)\n");

            }
            if(authorized){

            //FileWriter for logging
            FileWriter logPw = new FileWriter("logfile", true);
            String logInput = "";

            String clientMsg = null;
            while ((clientMsg = in.readLine()) != null) {


              //BEGIN loop
              //----------------------------------------------------------------------

              if(clientMsg.equals("GET") || clientMsg.length() >= 6){
			    if (clientMsg.equals("Init Server")){
                    //Used once, fetches the rights of a user
                    out.println(getProperty(subjectName, INDEX_ACCESS_LEVEL));
                    logInput = subjectName + " connected to the server";


                }else if (clientMsg.equals("GET")){
                    if (rights[0]){
                        logInput = "accepted GET request by " + subjectName;
                        String ret = "";
                        if (rights[1]){
                            String department = getProperty(subjectName,INDEX_DEPARTMENT);
                            System.out.println(department + " : " + subjectName);
                            ret = findAllDepartment(department,subjectName);
                            if (rights[2]){
                                out.println(ret +"divider"+ getMedicalRecord(subjectName, ret,INDEX_RECORD_DOCTOR));
                            }else{
                              out.println(ret +"divider"+ getMedicalRecord(subjectName, ret,INDEX_RECORD_NURSE));
                            }

                        }else{
                            if (rights[2]){
                                out.println(getAll());
                            }else{
                                out.println(getProperty(subjectName, INDEX_RECORD));
                            }
                        }
                    } else {
                        logInput = "failed GET request by " + subjectName;
                    }
                }else if(clientMsg.substring(0,6).equals("CHANGE")){

                    if (rights[1]){
                        //System.out.println("INDEX:NAME:MESSAGE"); // SYNTAX
                        String command = clientMsg.substring("CHANGE:".length(),clientMsg.length());
                        System.out.println("Current command"+command);
                        String[] entries = command.split(":");
                        //setProperty(entries);
                        try{//simpleWriteToFile(entries);
                            setProperty(entries);
                            out.println("Wrote to file");
                            logInput = "accepted CHANGE request by " + subjectName;
                        }catch(Exception e){
                            out.println("Failed writing to file");
                            logInput = "CHANGE request failed by " + subjectName;
                        }
                    }else{
                        out.println("You lack access to this function");
                        logInput = "failed CHANGE request by " + subjectName;
                    }
                }else if (clientMsg.substring(0,6).equals("CREATE")){
                    if (rights[2]){
                        String command = clientMsg.substring("CREATE:".length(),clientMsg.length());
                        String[] entries = command.split(":");
                        try{
                          appendRecord(entries);
                        out.println("Created new entry");
                        logInput = "accepted CREATE request by " + subjectName;
                      }catch(Exception e){
                        out.println("Failed to create a new entry");
                        logInput = "rejected CREATE request by " + subjectName;
                      }
                    }else{
                        out.println("You lack access to this function");
                        logInput = "failed CREATE request by " + subjectName;
                    }
                }else if(clientMsg.substring(0,6).equals("DELETE")){
                    if (rights[0] && rights[2] && !rights[1]){
                        String command = clientMsg.substring("DELETE:".length(),clientMsg.length());
                        String[] entries = command.split(":");
                        if(deleteMethod(entries)){
                            out.println("Deleted entry");
                        }else{
                            out.println("record not exist");
                        }
                        logInput = "accepted DELETE request by " + subjectName;
                    }else{
                        out.println("Failed to delete");
                        logInput = "failed DELETE request by " + subjectName;
                    }
                }else{
                  out.println("Command not recognized");
                  logInput = "failed command, not recognized. Request by " + subjectName;
                }
                logPw.append(logInput + ";timestamp: " + System.currentTimeMillis() + "\n");
                logPw.flush();

				out.flush();
                System.out.println("request recieved and response sent.\n");
			}else{
        out.println("Command not recognized");
        logInput = "failed command, not recognized. Request by " + subjectName;
        logPw.append(logInput + ";timestamp: " + System.currentTimeMillis() + "\n");
        logPw.flush();
      }
    }
            logPw.close();
			in.close();
			out.close();
			socket.close();
    	    numConnectedClients--;
            System.out.println("client disconnected");
            System.out.println(numConnectedClients + " concurrent connection(s)\n");}
		} catch (IOException e) {
            System.out.println("Client died: " + e.getMessage());
            e.printStackTrace();
            return;
        }
    }

    public boolean deleteMethod(String[] entries){
      try{
      BufferedReader reader = new BufferedReader(new FileReader(new File(filepath)));
      BufferedWriter writer = new BufferedWriter(new FileWriter(new File("filepath.txt")));
      int recordToRemove = Integer.valueOf(entries[1]);
      String currentLine;
      while((currentLine = reader.readLine()) != null){
        if (currentLine.startsWith(entries[0])){
          String[] line = currentLine.split(", ");
          String[] records = line[INDEX_RECORD].split(":");
          if (recordToRemove>records.length-1){
            return false;
          }else{
            records[recordToRemove]="";
          }
          writer.write(line[0]+", "+line[1]+", "+line[2]+", ");
          String temp ="";
          for (int n = 0;n<records.length ;n++ ) {
            if (n!=recordToRemove){
              temp+=records[n]+";";
            }
          }
          temp = temp.substring(0, temp.length() - 1);
          writer.write(temp);
          writer.newLine();


        }else{
          writer.write(currentLine);
          writer.newLine();
        }
      }

      reader.close();
      writer.close();
      return true;
    }
    catch(Exception e){
      e.printStackTrace();
      return false;
    }

    }

    /**
    * Much simpler (and dumber) version of writing to file
    *
    */
    public void simpleWriteToFile(String[] entries) throws Exception{
        String fileContents = readAllFromFile(filepath);
        fileContents= fileContents.replace(entries[COMMAND_OLD],entries[COMMAND_NEW]);

        try {
            File file = new File(filepath);
            FileWriter fileWriter = new FileWriter(file);
            fileWriter.write(fileContents);
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getAll(){
        String getAll = ""; // kompilator
        String ret="";
        try{
            getAll = readAllFromFile(filepath);
        }catch(Exception e){}

        String[] lineSplit = getAll.split("\n");
        for (int n = 0; n<lineSplit.length; n++){
            String[] lineEntry = lineSplit[n].split(", ");
            if(!lineEntry[INDEX_RECORD].contains("placeholder")){
                ret+=lineEntry[INDEX_RECORD]+":";
                //System.out.print(lineEntry[INDEX_RECORD]+":");
            }



        }
        ret=ret.replace("\n","");
        return ret.substring(0,ret.length() - ":".length());

    }

    public String setProperty(String[] entries)throws Exception{
        String getAll = ""; // kompilator
        try{
            getAll = readAllFromFile(filepath);
        }catch(Exception e){}

        String[] lineSplit = getAll.split("\n");
        for (int n = 0; n<lineSplit.length; n++){
            String[] lineEntry = lineSplit[n].split(", ");
            if (lineEntry[INDEX_NAME].equals(entries[COMMAND_NAME])){

                String[] indivrecords = lineEntry[INDEX_RECORD].split(":");
                String[] entry = indivrecords[Integer.valueOf(entries[COMMAND_INDEX])].split(";");
                entry[INDEX_RECORD_PLAINTEXT] = entries[COMMAND_PLAINTEXT];
                if (writeToFile(n,lineSplit,lineEntry,indivrecords,entry,Integer.valueOf(entries[COMMAND_INDEX]))){
                    return "Changed entry in database";
                }else{
                    return "Was meant to change in database, blame alcohol for fuckin' it up";
                }



            }
        }



        throw new Exception();

    }


    public boolean writeToFile(int number, String[] lines, String[] PersonLine, String[] PersonRecord, String[] entries,int rec){

        StringBuilder str = new StringBuilder();

        for (int i = 0;i < number; i++) {
            str.append(lines[i]+"\n");//people
        }
        for (int i = 0;i < PersonLine.length - 1; i++ ) { //person
            str.append(PersonLine[i]+ ", ");
        }
        for (int i = 0;i < PersonRecord.length; i++ ) {
          if(i == rec){
            for (int n = 0; n < entries.length ;n++ ) {
                //System.out.println(entries[n]);
                str.append(entries[n] + ";"  );
            }
            str.deleteCharAt(str.length()-1);
            str.append(":");
          }else{
              str.append(PersonRecord[i] + ":"  );
          }


        }
        str.deleteCharAt(str.length()-1);
        str.append("\n");

        if((number+1) < lines.length){

            for (int i = number+1;i < lines.length; i++) {
              str.append(lines[i]+"\n");
            }
        }
        try{
          BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filepath)));
          //System.out.println(str.toString());
          writer.write(str.toString());
          writer.close();

          return true;
      }catch(Exception e){
        e.printStackTrace();
        return false;
      }
    }


    /**
    * This method adds a new record att the end of the correct line.
    */
    private void appendRecord(String[] entries)throws Exception{
        String search=""; //kompilator semantics
        boolean appended = false;
    try {search = readAllFromFile(filepath);
    } catch(IOException e){}
    String[] lines = search.split("\n");
    for (int n = 0; n<lines.length; n++) {
        if (lines[n].contains(entries[0])){
            lines[n] += ":"+entries[1];
            appended= true;
        }
    }
    if(!appended) throw new Exception();
    try {
            File file = new File(filepath);
            FileWriter fileWriter = new FileWriter(file);
            for (String line: lines) {
                fileWriter.write(line + "\n");
            }
            fileWriter.flush();
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
    * This functions determines which records the current client has the right to write.
    * @param The name of the subject
    * @param The full records for this division, to be tampered with
    * @param To distinguish between nurse and doctor
    * @return A string containing the index, name and record
    */
    public String getMedicalRecord(String name, String allRecords, int myAccessLevel){
        String returnString="";
        if (allRecords.equals(""))
            return returnString;
        String[] splitByPatient = allRecords.split(divider);
        for (String patient : splitByPatient) {
            String[] splitByEntry = patient.split(", ");
            if (splitByEntry[INDEX_ACCESS_LEVEL].equals("4")){
            //System.out.println(patient);
            String[] recordEntries = splitByEntry[INDEX_RECORD].split(":");
            for (int n = 0; n<recordEntries.length; n++) {

                String[] indivrecords = recordEntries[n].split(";");

                if (indivrecords[myAccessLevel].equals(name) ){
                    returnString += "startOfEntry" + n +":" + indivrecords[INDEX_RECORD_PATIENT] + ":" + indivrecords[INDEX_RECORD_PLAINTEXT];
                }
            }}
        }
       return returnString;
        //Yo dawg, I heard you like parsing
    }

    /**
    * getProperty searches by the name, name is parsed such
    * that only and only the name is in name-variable
    *
    * @return The type of property a certain person should get
    * @param Only the name to search for
    * @param What property to search for
    */
    public String getProperty(String name, int index){
        String search=""; //kompilator semantics
    try {search = readAllFromFile(filepath);
    } catch(IOException e){}
    String[] rwx = search.split("\n");
    for (String s: rwx) {
        String[] splits=s.split(", ");

    if (splits[INDEX_NAME].equals(name)) return splits[index];
    }
        return "NAN";
    }

    /**
    * This method returns every record for a certain department
    *
    */
    public String findAllDepartment(String dep, String subjectName){
        String ret = "";

        String search=""; //kompilator semantics
        try {search = readAllFromFile(filepath);
        } catch(IOException e){}
        String[] rwx = search.split("\n");
        for (String s: rwx) {
            String[] splits=s.split(", ");
            if (splits[INDEX_DEPARTMENT].equals(dep) && !splits[INDEX_NAME].equals(subjectName))ret+= s + divider;
        }
        if (ret.toCharArray().length < divider.length())
            return "";
        return ret.substring(0,ret.length() - divider.length());
    }


    /**
    * Some stock filereader i had lying around. It's very nice.
    * Takes all text and returns it as one long String.
    * Very powerful yet very costly(note)
    * @return One string containing all text, use .split(arg)
    * @param filepath
    */
    public static String readAllFromFile(String path) throws IOException {
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        byte[] data = new byte[(int) file.length()];
        fis.read(data);
        fis.close();

        String str = new String(data, "UTF-8");
        return str;
    }

    /**
    * Parsing priviledges into a nice string for displaying
    * May be obsolete
    * But it looks nice :D
    */
    private String parsePriviledgeToString(String number){
        String ret ="";
        if (Integer.valueOf(number)>0 && Integer.valueOf(number) < 8){
        byte currentPriviledge = Byte.parseByte(number); //binary

            if ((currentPriviledge & 0b100) == 0b100){
                ret+="read/";
            }
            if ((currentPriviledge & 0b10) == 0b10){
                ret+="write/";
            }
            if ((currentPriviledge & 0b1) == 0b1){
                ret+="execute/";
            }
            return ret.substring(0, ret.length() - 1);
        }
        return "N/A";
    }

    /**
    * Actuall logic, sets boolean values for use later
    */
    private void setPriviledges(String number, boolean[] rights)throws Exception{
      if(number.equals("NAN")) throw new Exception();
        if (Integer.valueOf(number)>0 && Integer.valueOf(number) < 8){
        byte currentPriviledge = Byte.parseByte(number); //binary

            if ((currentPriviledge & 0b100) == 0b100){
                rights[0] = true;
            }
            if ((currentPriviledge & 0b10) == 0b10){
                rights[1] = true;
            }
            if ((currentPriviledge & 0b1) == 0b1){
                rights[2] = true;
            }

        }
    }

    private void newListener() { (new Thread(this)).start(); } // calls run()

    public static void main(String args[]) {
        System.out.println("\nServer Started\n");
        int port = 9876;
        String type = "TLS";
        try {
            ServerSocketFactory ssf = getServerSocketFactory(type);
            ServerSocket ss = ssf.createServerSocket(port);
            ((SSLServerSocket)ss).setNeedClientAuth(true); // enables client authentication
            new server(ss);
        } catch (IOException e) {
            System.out.println("Unable to start Server: " + e.getMessage());
            e.printStackTrace();
        }
    }

    /**
    * Factory stuff, dont mind it
    */
    private static ServerSocketFactory getServerSocketFactory(String type) {
        if (type.equals("TLS")) {
            SSLServerSocketFactory ssf = null;
            try { // set up key manager to perform server authentication
                SSLContext ctx = SSLContext.getInstance("TLS");
                KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
                TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
                KeyStore ks = KeyStore.getInstance("JKS");
				KeyStore ts = KeyStore.getInstance("JKS");
                char[] password = "password".toCharArray();

                ks.load(new FileInputStream("keys/serverkeystore"), password);  // keystore password (storepass)
                ts.load(new FileInputStream("keys/servertruststore"), password); // truststore password (storepass)
                kmf.init(ks, password); // certificate password (keypass)
                tmf.init(ts);  // possible to use keystore as truststore here
                ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
                ssf = ctx.getServerSocketFactory();
                return ssf;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            return ServerSocketFactory.getDefault();
        }
        return null;
    }
}
