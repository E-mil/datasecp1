https://docs.google.com/presentation/d/1LIOj9oQrXJGjeebLu5_SI4reUx5WDjzuFEs9Muft1Ek/edit?usp=sharing

Presentation^

https://drive.google.com/drive/folders/1NgYwcb7r2ELp2QjvBTtmQLfdrWaHDCQE?usp=sharing

peer review^

--- 

skapa nya keystores/truststores

Skriv en klient som kan byta "user" enkelt (GUI?)


* [X] two-factor-authentication? SSL and password(?) <- Ändra ifall vi inte vill ha 'password' som lösen på alla


---

Front page with the names of the group members. (1 page) 

• High-level architectural overview. (2 pages)

• [X] Ethical discussion. (1 page) (Någon får gärna läsa igenom och kolla så att allt ser ok ut)

• Security evaluation of the design. (2-3 pages)

• Peer-reviews of your report. (2 pages)

• Improvement sheet. (1 page)

• Signed functionality review form. (1 page)

• Signed contribution statement form. (1 page)

Länk till dokument:

https://www.overleaf.com/13816998rfmqwcgtbcpf






---
FORMATERING I MEDICALRECORD.TXT


LINE_ENTRY = {NAMN, ACCESS_LEVEL, DEPARTMENT, RECORD1:RECORD2:osv}

RECORD_ENTRY = {PATIENT;NURSE;DOCTOR;PLAINTEXT(status)}

DONT USE comma in plaintext

Subject to change

---

THE API

Alla anrop ser ut; 
>out.println("dittkommandohär:eventuellpayloadhär");
>out.flush;

Finns 4 kommando. GET, CHANGE, CREATE, DELETE

GET ser ändast ut; out.println("GET"); <-caps

CHANGE ser ut; out.println("CHANGE:den gammla strängen:den nya strängen");

CREATE ser ut; out.println("CREATE:SökNamn:HELA NYA RECORD:et(view record_entry)");

yet to implement delete

Tanken är att GET returnerar det som finns i databasen och att CHANGE,CREATE,DELETE bara returnerar status-msg.
Dvs efter varje ändring kommer GET behövas skickas igen för att clienten ska ha uppdaterad info.


